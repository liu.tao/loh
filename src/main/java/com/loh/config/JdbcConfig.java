package com.loh.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by liutao on 17-4-15.
 */
@PropertySource(value = "application.properties")
@Component()
public class JdbcConfig {

    /** * 数据库用户名 */
    @Value("${spring.datasource.username}")
    private  String userName="root";
    /** * 驱动名称 */
    @Value("${spring.datasource.driverClassName}")
    private String driverClass="com.mysql.jdbc.Driver";
    /** * 数据库连接url */
    @Value("${spring.datasource.url}")
    private  String url="jdbc:mysql://127.0.0.1:3306/loh?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
    /** * 数据库密码 */
    @Value("${spring.datasource.password}")
    public  String password="root";

    public  String getUserName() {
        return userName;
    }

    public  String getDriverClass() {
        return driverClass;
    }

    public  String getUrl() {
        return url;
    }

    public  String getPassword() {
        return password;
    }

}
