package com.loh.dao;

import com.loh.model.Lease;
import com.loh.model.LeaseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LeaseMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    long countByExample(LeaseExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int deleteByExample(LeaseExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int insert(Lease record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int insertSelective(Lease record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    List<Lease> selectByExample(LeaseExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    Lease selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Lease record, @Param("example") LeaseExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Lease record, @Param("example") LeaseExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Lease record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lease
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Lease record);
}