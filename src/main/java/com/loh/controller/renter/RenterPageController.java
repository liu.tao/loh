package com.loh.controller.renter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by liutao on 17-4-20.
 */
@Controller
public class RenterPageController {

    // -----------console 租客管理页面--------------
    @RequestMapping("/renter/console/index")
    public String console() {
        return "/renter/console/index";
    }

    // 工单管理
    @RequestMapping("/renter/console/renterWorkOrder")
    public String renterWorkOrder() {
        return "/renter/console/renterWorkOrder";
    }

    // 创建工单
    @RequestMapping("/renter/console/renterCreateWorkOrder")
    public String renterCreateWorkOrder() {
        return "/renter/console/renterCreateWorkOrder";
    }

    // 个人信息
    @RequestMapping("/renter/console/renterInfo")
    public String renterInfo() {
        return "/renter/console/renterInfo";
    }
    // 实名认证

    @RequestMapping("/renter/console/renterCertification")
    public String renterCertification() {
        return "/renter/console/renterCertification";
    }

    // 合同管理
    @RequestMapping("/renter/console/renterContract")
    public String renterContract() {
        return "/renter/console/renterContract";
    }

    // --------------house 租客前端页面展示------------
    @RequestMapping("/renter/house")
    public String house() {
        return "/renter/house";
    }
    //首页跳转
    @RequestMapping("/renter/index")
    public String index() {
        return "/renter/index";
    }

}
