package com.loh.controller.renter;

import com.loh.model.HouseInfoDetail;
import com.loh.service.renter.ShowHouseService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

import java.util.List;

/**
 * Created by liutao on 17-4-24.
 */
@Controller
public class ShowHouseInfoController {
    @Resource
    private ShowHouseService showHouseService;

    @RequestMapping(value = "renter/house", method = RequestMethod.GET)
    public String showHouseInfo(Model model, @ModelAttribute(value = "HouseInfo") HouseInfoDetail houseInfoDetail) {
        List<HouseInfoDetail> houseInfoDetailList = showHouseService.showHouseDetail(houseInfoDetail);
        model.addAttribute("houseInfoList", houseInfoDetailList);
        return "renter/house";
    }

    @RequestMapping(value = "renter/houseDetail", method = RequestMethod.GET)
    public String showHouseDetail(Model model, @ModelAttribute(value = "HouseInfo") HouseInfoDetail houseInfo) {
        List<HouseInfoDetail> houseInfoDetailList = showHouseService.showHouseDetail(houseInfo);
        if (!CollectionUtils.isEmpty(houseInfoDetailList)){
            model.addAttribute("houseInfo", houseInfoDetailList.get(0));
        }
        return "renter/houseDetail";
    }

}
