package com.loh.controller.renter;

import com.loh.model.ContractDetail;
import com.loh.model.User;
import com.loh.service.landlord.LContractManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-20.
 */
@Controller
public class RenterContractController {

    @Resource
    private LContractManageService lContractManageService;

    @RequestMapping(value = "renter/console/renterContract", method = RequestMethod.GET)
    public String showRenterContract(HttpServletRequest request, Model model) {
        try {
            HttpSession httpSession = request.getSession();
            ContractDetail contractDetail = new ContractDetail();
            User renter = (User) httpSession.getAttribute("user");
            contractDetail.setRenterId(renter.getId());
            List<ContractDetail> contractDetails = lContractManageService.showContract(contractDetail);
            model.addAttribute("contractDetails", contractDetails);
        } catch (Exception e) {

        }
        return "renter/console/renterContract";
    }

}
