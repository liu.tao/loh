package com.loh.controller.renter;

import com.loh.model.User;
import com.loh.model.UserInfo;
import com.loh.service.admin.UserManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-20.
 */
@Controller
public class RenterInfoManageController {

    @Resource
    private UserManageService userManageService;

    @RequestMapping(value = "renter/console/renterInfo", method = RequestMethod.GET)
    public String showRenterInfo(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        List<UserInfo> userInfoList = userManageService.selectRenter(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        return "renter/console/renterInfo";
    }

    @RequestMapping(value = "renter/console/renterInfo/update", method = RequestMethod.GET)
    public String updateRenterInfo(Model model, HttpServletRequest request,
            @ModelAttribute(value = "UserInfo") UserInfo userInfo) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        user.setUsername(null);
        userInfo.setId(user.getId());
        Integer success = userManageService.updateRent(userInfo);
        List<UserInfo> userInfoList = userManageService.selectRenter(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        model.addAttribute("success",success);
        return "renter/console/renterInfo";
    }

    @RequestMapping(value = "renter/console/renterCertification", method = RequestMethod.POST)
    public String renterCertification(Model model, HttpServletRequest request,
                                   @ModelAttribute(value = "UserInfo") UserInfo userInfo) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        user.setUsername(null);
        userInfo.setId(user.getId());
        Integer success = userManageService.uploadRentCertificationImage(userInfo);
        List<UserInfo> userInfoList = userManageService.selectRenter(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        model.addAttribute("success",success);
        return "renter/console/renterInfo";
    }

}
