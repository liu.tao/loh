package com.loh.controller;

import com.loh.model.User;
import com.loh.service.admin.LoginService;
import com.loh.service.admin.UserManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class LoginController {
    @Resource
    private LoginService loginService;
    @Resource
    private UserManageService userManageService;

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(Model model, HttpServletRequest request, @ModelAttribute(value = "user") User user) {
        Integer role = Integer.valueOf(request.getParameter("role"));
        user.setRoleId(role);
        String message = null;
        if (user.getEmail() != null && user.getPassword() != null) {

            boolean status = loginService.login(user);
            if (status) {
                user = userManageService.getUSerByCriteria(user);
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                if (role == 1) {
                    return "admin/index";
                }
                if (role == 2) {
                    return "renter/index";
                }
                if (role == 3) {
                    return "landlord/index";
                }
            } else {
                message = "帐号／密码错误";
            }
        } else {
            message = "帐号密码不能为空";
        }
        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return "/index";
    }

}
