package com.loh.controller;

import com.loh.dao.UserMapper;
import com.loh.model.User;
import com.loh.model.UserExample;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-11.
 */
@Controller
public class HelloController {
    @Resource
    private UserMapper userMapper;


    @RequestMapping("/modelAttribute")
    public String modelAttribute(Model model){
        model.addAttribute("test1", "测试信息1");
        return "modelAttribute";
    }

}
