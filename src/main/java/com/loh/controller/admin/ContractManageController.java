package com.loh.controller.admin;

import com.loh.model.ContractType;
import com.loh.service.admin.ContractManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class ContractManageController {
@Resource
    private ContractManageService contractManageService;
@RequestMapping(value = "admin/contractManage", method = RequestMethod.GET)
    public String selectContractType(Model model){
    List<ContractType> contractTypeList=contractManageService.selectContractType();
    model.addAttribute("contractTypes",contractTypeList);
    return "admin/contractManage";
}

}
