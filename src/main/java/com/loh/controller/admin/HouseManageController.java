package com.loh.controller.admin;

import com.loh.model.HouseInfoDetail;
import com.loh.model.WorkOrderInfo;
import com.loh.service.admin.HouseManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class HouseManageController {
    @Resource
    private HouseManageService houseManageService;

    @RequestMapping(value = "admin/houseManage", method = RequestMethod.GET)
    public String showLandlord(Model model ,@ModelAttribute(value = "HouseInfoDetail") HouseInfoDetail houseInfoDetail) {

        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(houseInfoDetail);
        model.addAttribute("houseInfos", houseInfoDetailList);
        return "admin/houseManage";
    }
    @RequestMapping(value = "admin/houseMaterial", method = RequestMethod.GET)
    public String showLandlordForMaterial(Model model) {

        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(new HouseInfoDetail());
        model.addAttribute("houseInfos", houseInfoDetailList);
        return "admin/houseMaterial";
    }
    @RequestMapping(value="admin/houseManage/check" ,method = RequestMethod.GET)
       public String checkHouse(Model model,@ModelAttribute(value = "HouseInfoDetail") HouseInfoDetail houseInfoDetail)
    {
        Integer success=houseManageService.updateHouseInfo(houseInfoDetail);
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(new HouseInfoDetail());
        model.addAttribute("houseInfos", houseInfoDetailList);
        model.addAttribute("success",success);
        return "admin/houseManage";
    }

    @RequestMapping(value="admin/houseManage/upload",method = RequestMethod.POST)
    public String uploadMaterial(Model model, HttpServletRequest request, @ModelAttribute(value = "HouseInfoDetail") HouseInfoDetail houseInfoDetail){
        Integer success=houseManageService.uploadMaterial(houseInfoDetail);
        //重新绑定下拉框
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(new HouseInfoDetail());
        model.addAttribute("houseInfos", houseInfoDetailList);
        model.addAttribute("success",success);
        return  "admin/houseMaterial";
    }




}
