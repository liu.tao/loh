package com.loh.controller.admin;

import com.loh.model.HouseInfoDetail;
import com.loh.model.WorkOrder;
import com.loh.model.WorkOrderInfo;
import com.loh.service.admin.WorkOrderManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class WorkOrderManageController {
    @Resource
    private WorkOrderManageService workOrderManageService;

    @RequestMapping(value = "admin/workOrder", method = RequestMethod.GET)
    public String showLandlord(Model model,@ModelAttribute(value = "WorkOrderInfo") WorkOrderInfo workOrderInfo) {
        List<WorkOrderInfo> workOrderList = workOrderManageService.selectWorkOrder(workOrderInfo);
        model.addAttribute("workOrders", workOrderList);
        return "admin/workOrder";
    }



    @RequestMapping(value = "admin/replyWorkOrder", params = "id", method = RequestMethod.GET)
    public String showWorkOrder(Model model, Integer id) {
        WorkOrderInfo workOrderInfo = new WorkOrderInfo();
        workOrderInfo.setId(id);
        List<WorkOrderInfo> workOrderInfoList = workOrderManageService.selectWorkOrder(workOrderInfo);
        if (!CollectionUtils.isEmpty(workOrderInfoList)) {
            workOrderInfo = workOrderInfoList.get(0);
        }
        model.addAttribute("workOrderInfo", workOrderInfo);
        return "admin/replyWorkOrder";
    }

    @RequestMapping(value = "admin/replyWorkOrder/update", method = RequestMethod.POST)
    public String replyWorkOrder(Model model, @ModelAttribute(value = "WorkOrder") WorkOrder workOrder) {
        Integer sum = workOrderManageService.replayWorkOrder(workOrder);
        List<WorkOrderInfo> workOrderList = workOrderManageService.selectWorkOrder(new WorkOrderInfo());
        model.addAttribute("workOrders", workOrderList);
        return "admin/workOrder";
    }

}
