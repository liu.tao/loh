package com.loh.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class AdminPageController {
    // admin
    @RequestMapping("/admin/landlordManage")
    public String landlordManage(Model model) {
        return "/admin/landlordManage";
    }

    @RequestMapping("/admin/renterManage")
    public String renterManage(Model model) {
        return "/admin/renterManage";
    }

    @RequestMapping("/admin/workOrder")
    public String workOrder(Model model) {
        return "/admin/workOrder";
    }

    @RequestMapping("/admin/houseManage")
    public String houseManage() {

        return "/admin/houseManage";
    }

    @RequestMapping("/admin/houseMaterial")
    public String houseMaterial() {

        return "/admin/houseMaterial";
    }

    @RequestMapping("/admin/contractManage")
    public String contractManage() {

        return "/admin/contractManage";
    }
}
