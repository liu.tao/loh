package com.loh.controller.admin;

import com.loh.model.HouseInfoDetail;
import com.loh.model.User;
import com.loh.model.UserInfo;
import com.loh.service.admin.UserManageService;
import org.apache.xpath.operations.Mod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class UserManageController {
    @Resource
    private UserManageService userManageService;

    @RequestMapping(value = "admin/renterManage", method = RequestMethod.GET)
    public String showRenter(Model model, @ModelAttribute(value = "User") User user) {
        List<UserInfo> userInfoList = userManageService.selectRenter(user);
        model.addAttribute("userInfos", userInfoList);
        return "admin/renterManage";
    }

    @RequestMapping(value = "admin/renter/update", method = RequestMethod.GET)
    public String updateRenter(Model model, @ModelAttribute(value = "User") User user) {
        Integer success = userManageService.updateUser(user);
        List<UserInfo> userInfoList = userManageService.selectRenter(new User());
        model.addAttribute("userInfos", userInfoList);
        model.addAttribute("success",success);
        return "admin/renterManage";
    }

    @RequestMapping(value = "admin/landlordManage", method = RequestMethod.GET)
    public String showLandlord(Model model, @ModelAttribute(value = "User") User user) {
        List<UserInfo> userInfoList = userManageService.selectLandlord(user);
        model.addAttribute("userInfos", userInfoList);

        return "admin/landlordManage";
    }

    @RequestMapping(value = "admin/landlord/update", method = RequestMethod.GET)
    public String updateLandlord(Model model, @ModelAttribute(value = "User") User user) {
        Integer success = userManageService.updateUser(user);
        List<UserInfo> userInfoList = userManageService.selectLandlord(new User());
        model.addAttribute("userInfos", userInfoList);
        model.addAttribute("success",success);
        return "admin/landlordManage";
    }

}
