package com.loh.controller.landlord;

import com.loh.model.HouseInfoDetail;
import com.loh.model.HouseType;
import com.loh.model.User;
import com.loh.model.WorkOrder;
import com.loh.service.admin.HouseManageService;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class LandlordHouseManageController {
    @Resource
    private HouseManageService houseManageService;


    @RequestMapping(value = "landlord/houseManage", method = RequestMethod.GET)
    public String showHouse(Model model, HttpServletRequest request) {
        HouseInfoDetail houseInfoDetail = new HouseInfoDetail();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        houseInfoDetail.setLandlordId(user.getId());
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(houseInfoDetail);
        model.addAttribute("houseInfos", houseInfoDetailList);
        return "landlord/houseManage";
    }

    @RequestMapping(value = "landlord/houseManage/criteria", method = RequestMethod.GET)
    public String showHouseCriteria(Model model, HttpServletRequest request,@ModelAttribute(value = "HouseInfoDetail") HouseInfoDetail houseInfoDetail) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        houseInfoDetail.setLandlordId(user.getId());
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(houseInfoDetail);
        model.addAttribute("houseInfos", houseInfoDetailList);
        return "landlord/houseManage";
    }



    @RequestMapping(value = "/landlord/houseApply", method = RequestMethod.GET)
    public String getHouseType(Model model) {
        List<HouseType> houseTypeList = houseManageService.selectHouseTypeList();
        model.addAttribute("houseTypes", houseTypeList);
        return "/landlord/houseApply";
    }

    @RequestMapping(value = "/landlord/houseApply/insert", method = RequestMethod.POST)
    public String applyLeaseHouse(Model model, HttpServletRequest request,
            @ModelAttribute(value = "HouseInfoDetail") HouseInfoDetail houseInfoDetail) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        houseInfoDetail.setLandlordId(user.getId());
        Integer success = houseManageService.insertHouseInfo(houseInfoDetail);
        // 重新绑定数据
        HouseInfoDetail houseInfoDetailSelect=new HouseInfoDetail();
        houseInfoDetail.setLandlordId(user.getId());
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(houseInfoDetailSelect);
        model.addAttribute("houseInfos", houseInfoDetailList);
        model.addAttribute("success", success);

        return "landlord/houseManage";
    }

}
