package com.loh.controller.landlord;

import com.loh.model.HouseInfoDetail;
import com.loh.model.User;
import com.loh.model.UserInfo;
import com.loh.service.admin.UserManageService;
import com.loh.service.landlord.LUserInfoService;
import org.apache.xpath.operations.Mod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class LandlordInfoManageController {
    @Resource
    private UserManageService userManageService;

    @RequestMapping(value = "landlord/landlordInfo", method = RequestMethod.GET)
    public String showLandlordInfo(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        List<UserInfo> userInfoList = userManageService.selectLandlord(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        return "landlord/landlordInfo";
    }
    @RequestMapping(value = "landlord/landlordInfo/update", method = RequestMethod.GET)
    public String updateLandlordInfo(Model model, HttpServletRequest request,@ModelAttribute(value = "UserInfo") UserInfo userInfo) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        user.setUsername(null);
        userInfo.setId(user.getId());

        Integer success=userManageService.updateLandlord(userInfo);
        //刷新页码数据
        List<UserInfo> userInfoList = userManageService.selectLandlord(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        model.addAttribute("success",success);
        return "landlord/landlordInfo";
    }

    @RequestMapping(value = "landlord/landlordCertification", method = RequestMethod.POST)
    public String landlordCertification(Model model, HttpServletRequest request,
                                      @ModelAttribute(value = "UserInfo") UserInfo userInfo) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        user.setUsername(null);
        userInfo.setId(user.getId());
        Integer success = userManageService.uploadLandlordCertificationImage(userInfo);
        List<UserInfo> userInfoList = userManageService.selectLandlord(user);
        model.addAttribute("landlordInfo", userInfoList.get(0));
        model.addAttribute("success",success);
        return "landlord/landlordInfo";
    }

}
