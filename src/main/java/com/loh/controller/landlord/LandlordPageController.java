package com.loh.controller.landlord;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by liutao on 17-4-16.
 */
@Controller
public class LandlordPageController {
    // 房屋申请
    @RequestMapping("/landlord/houseApply")
    public String houseApply() {
        return "/landlord/houseApply";
    }

    // 房屋管理
    @RequestMapping("/landlord/houseManage")
    public String houseManage() {
        return "/landlord/houseManage";
    }

    // 房东信息
    @RequestMapping("/landlord/landlordInfo")
    public String landlordInfo() {
        return "/landlord/landlordInfo";
    }

    // 合同管理
    @RequestMapping("/landlord/landlordContractManage")
    public String landlordContractManage() {
        return "/landlord/landlordContractManage";
    }

    // 合同上传
    @RequestMapping("/landlord/uploadContract")
    public String uploadContract() {
        return "/landlord/uploadingContract";
    }

    // 工单管理
    @RequestMapping("/landlord/landlordWorkOrder")
    public String landlordWorkOrder() {
        return "/landlord/landlordWorkOrder";
    }

    // 创建工单
    @RequestMapping("/landlord/createWorkOrder")
    public String creatWorkOrder() {
        return "/landlord/createWorkOrder";
    }

    // 房东认证
    @RequestMapping("/landlord/landlordCertification")
    public String landlordCertification() {
        return "/landlord/landlordCertification";
    }

    // 租赁管理
    @RequestMapping("/landlord/leaseManage")
    public String leaseManage() {
        return "/landlord/leaseManage";
    }

}
