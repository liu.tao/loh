package com.loh.controller.landlord;

import com.loh.model.HouseInfoDetail;
import com.loh.model.User;
import com.loh.model.WorkOrder;
import com.loh.model.WorkOrderInfo;
import com.loh.service.admin.WorkOrderManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class landlordWorkOrderController {
    @Resource
    private WorkOrderManageService workOrderManageService;

    @RequestMapping(value = "landlord/landlordWorkOrder", method = RequestMethod.GET)
    public String showWorkOrder(Model model, HttpServletRequest request,
            @ModelAttribute(value = "WorkOrderInfo") WorkOrderInfo workOrderInfo) {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        workOrderInfo.setUserId(user.getId());
        List<WorkOrderInfo> workOrderInfoList = workOrderManageService.selectWorkOrder(workOrderInfo);

        model.addAttribute("workOrders", workOrderInfoList);
        return "landlord/landlordWorkOrder";
    }

    @RequestMapping(value = "landlord/workOrderDetail", params = "id", method = RequestMethod.GET)
    public String showWorkOrderDetail(Model model, Integer id) {
        WorkOrderInfo workOrderInfo = new WorkOrderInfo();
        workOrderInfo.setId(id);
        List<WorkOrderInfo> workOrderInfoList = workOrderManageService.selectWorkOrder(workOrderInfo);
        if (!CollectionUtils.isEmpty(workOrderInfoList)) {
            workOrderInfo = workOrderInfoList.get(0);
        }
        model.addAttribute("workOrderInfo", workOrderInfo);
        return "landlord/workOrderDetail";
    }

    @RequestMapping(value = "landlord/workOrderDetail/insert", method = RequestMethod.POST)
    public String createWorkOrder(Model model, HttpServletRequest request,
            @ModelAttribute(value = "WorkOrder") WorkOrder workOrder) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        workOrder.setUserId(user.getId());
        Integer success = workOrderManageService.insertWorkOrder(workOrder);
        // 绑定数据
        WorkOrderInfo workOrderInfo = new WorkOrderInfo();
        workOrderInfo.setUserId(user.getId());
        List<WorkOrderInfo> workOrderInfoList = workOrderManageService.selectWorkOrder(workOrderInfo);
        model.addAttribute("success", success);
        model.addAttribute("workOrders", workOrderInfoList);
        return "landlord/landlordWorkOrder";

    }

}
