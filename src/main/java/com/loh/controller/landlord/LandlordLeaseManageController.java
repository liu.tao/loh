package com.loh.controller.landlord;

import com.loh.model.LeaseInfo;
import com.loh.model.User;
import com.loh.service.landlord.LLeaseManageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class LandlordLeaseManageController {
    @Resource
    private LLeaseManageService lLeaseManageService;

    @RequestMapping(value = "landlord/leaseManage", method = RequestMethod.GET)
    public String showLease(Model model, HttpServletRequest request) {
        try {
            HttpSession httpSession = request.getSession();
            User user = (User) httpSession.getAttribute("user");
            List<LeaseInfo> leaseInfoList = lLeaseManageService.selectLeaseInfo(user);
            model.addAttribute("leaseInfos", leaseInfoList);
        } catch (Exception e) {

        }
        return "landlord/leaseManage";
    }
}
