package com.loh.controller.landlord;

import com.loh.model.*;
import com.loh.service.admin.ContractManageService;
import com.loh.service.admin.HouseManageService;
import com.loh.service.landlord.LContractManageService;
import org.apache.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Controller
public class LandlordContractManageController {
    @Resource
    private LContractManageService lContractManageService;
    @Resource
    private HouseManageService houseManageService;
    @Resource
    private ContractManageService contractManageService;

    @RequestMapping(value = "landlord/landlordContractManage", method = RequestMethod.GET)
    public String showContract(HttpServletRequest request, Model model) {
        try {
            HttpSession httpSession = request.getSession();
            User user = (User) httpSession.getAttribute("user");
            ContractDetail contractDetail = new ContractDetail();
            contractDetail.setLandlordId(user.getId());
            List<ContractDetail> contractDetails = lContractManageService.showContract(contractDetail);
            model.addAttribute("contractDetails", contractDetails);
        } catch (Exception e) {

        }
        return "landlord/landlordContractManage";
    }

    @RequestMapping(value = "landlord/uploadContract", method = RequestMethod.GET)
    public String landlordContractManageDownList(Model model) {
        List<HouseInfoDetail> houseInfoDetailList = houseManageService.selectHouseInfoDetail(new HouseInfoDetail());
        model.addAttribute("houseInfos", houseInfoDetailList);
        List<ContractType> contractTypeList=contractManageService.selectContractType();
        model.addAttribute("contractTypes",contractTypeList);
        return "/landlord/uploadingContract";
    }

    @RequestMapping(value = "landlord/uploadContract/upload", method = RequestMethod.POST)
    public String landlordContractManageDownList(Model model,
            @ModelAttribute(value = "ContractDetail") ContractDetail contractDetail, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute("user");
        contractDetail.setLandlordId(user.getId());

        Integer success = lContractManageService.insertContract(contractDetail);
        ContractDetail contractDetailSelect = new ContractDetail();
        contractDetailSelect.setLandlordId(user.getId());
        List<ContractDetail> contractDetails = lContractManageService.showContract(contractDetailSelect);
        model.addAttribute("contractDetails", contractDetails);
        model.addAttribute("success", success);
        return "landlord/landlordContractManage";
    }

}
