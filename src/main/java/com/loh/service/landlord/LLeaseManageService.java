package com.loh.service.landlord;

import com.loh.model.LeaseInfo;
import com.loh.model.User;

import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
public interface LLeaseManageService {
    List<LeaseInfo> selectLeaseInfo(User param);
}
