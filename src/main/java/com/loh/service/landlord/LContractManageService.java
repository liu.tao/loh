package com.loh.service.landlord;

import com.loh.model.ContractDetail;
import com.loh.model.HouseInfo;
import com.loh.model.HouseInfoDetail;

import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
public interface LContractManageService {
    List<ContractDetail> showContract(ContractDetail param);

    Integer insertContract(ContractDetail param);

}
