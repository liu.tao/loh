package com.loh.service.landlord.impl;

import com.google.common.collect.Lists;
import com.loh.dao.LeaseMapper;
import com.loh.model.*;
import com.loh.service.admin.UserManageService;
import com.loh.service.landlord.LContractManageService;
import com.loh.service.landlord.LLeaseManageService;
import com.mysql.fabric.xmlrpc.base.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Service
public class LLeaseManageServiceImpl implements LLeaseManageService {
    @Resource
    private LeaseMapper leaseMapper;
    @Resource
    private UserManageService userManageService;
    @Resource
    private LContractManageService lContractManageService;

    @Override
    public List<LeaseInfo> selectLeaseInfo(User param) {
        List<LeaseInfo> leaseInfoList = Lists.newArrayList();
        LeaseExample leaseExample = new LeaseExample();
        LeaseExample.Criteria criteria = leaseExample.createCriteria();
        if (param.getId() != null) {
            criteria.andLandlordIdEqualTo(param.getId());
        }
        List<Lease> leaseList = leaseMapper.selectByExample(leaseExample);
        for (Lease lease : leaseList) {
            LeaseInfo leaseInfo = new LeaseInfo();
            leaseInfo.setStatus(lease.getStatus() == 1 ? "有效" : "无效");
            User user=new User();
            user.setId(lease.getRenterId());
            UserInfo renterInfo = userManageService.selectRenter(user).get(0);
            leaseInfo.setRenterName(renterInfo.getUsername());
            leaseInfo.setRenterEmail(renterInfo.getEmail());
            leaseInfo.setRenterPhone(renterInfo.getPhone());
            ContractDetail contractDetail = new ContractDetail();
            contractDetail.setRenterId(lease.getRenterId());
            contractDetail.setLandlordId(lease.getLandlordId());
            contractDetail = lContractManageService.showContract(contractDetail).get(0);
            leaseInfo.setEndTimeStr(contractDetail.getEndTimeStr());
            leaseInfo.setStartTimeStr(contractDetail.getStartTimeStr());
            leaseInfo.setContractType(contractDetail.getContractType());
            leaseInfo.setId(lease.getId());
            leaseInfoList.add(leaseInfo);
        }
        return leaseInfoList;
    }

}
