package com.loh.service.landlord.impl;

import com.google.common.collect.Lists;
import com.loh.dao.ContractMapper;
import com.loh.dao.HouseInfoMapper;
import com.loh.dao.UserMapper;
import com.loh.model.*;
import com.loh.service.admin.ContractManageService;
import com.loh.service.admin.UserManageService;
import com.loh.service.landlord.LContractManageService;
import com.loh.total.DateUtils;
import com.loh.total.QiniuUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by liutao on 17-4-18.
 */
@Service
public class LContractManageServiceImpl implements LContractManageService {
    @Resource
    private ContractMapper contractMapper;
    @Resource
    private UserManageService userManageService;
    @Resource
    private ContractManageService contractManageService;
    @Resource
    private UserMapper userMapper;
    @Resource
    private HouseInfoMapper houseInfoMapper;

    @Override
    public List<ContractDetail> showContract(ContractDetail param) {
        List<ContractDetail> contractDetailList = Lists.newArrayList();
        ContractExample contractExample = new ContractExample();
        ContractExample.Criteria criteria = contractExample.createCriteria();
        if (param.getLandlordId() != null) {
            criteria.andLandlordIdEqualTo(param.getLandlordId());
        }
        if (param.getRenterId() != null) {
            criteria.andRenterIdEqualTo(param.getRenterId());
        }
        List<Contract> contractList = contractMapper.selectByExample(contractExample);
        for (Contract contract : contractList) {
            ContractDetail contractDetail = new ContractDetail();
            contractDetail.setId(contract.getId());
            contractDetail.setFile(contract.getFile());
            if (contract.getRenterId() != null) {
                User renter = userManageService.getUserById(contract.getRenterId());
                contractDetail.setRenterName(renter.getUsername());
                contractDetail.setRenterPhone(renter.getPhone());
                contractDetail.setRenterEmail(renter.getEmail());
            }
            if (contract.getLandlordId() != null) {
                User landlord = userManageService.getUserById(contract.getLandlordId());
                contractDetail.setLandlordName(landlord.getUsername());
                contractDetail.setLandlordEmail(landlord.getEmail());
                contractDetail.setLandlordPhone(landlord.getPhone());
            }
            if (contract.getType() != null) {
                ContractType contractType = contractManageService.selectContractTypeById(contract.getType());
                contractDetail.setContractType(contractType.getName());
            }
            if (contract.getStartTime() != null && contract.getEndTime() != null) {
                contractDetail.setStartTimeStr(DateUtils.formatDateTime(contract.getStartTime()));
                contractDetail.setEndTimeStr(DateUtils.formatDateTime(contract.getEndTime()));
            }
            contractDetail.setFileStatus(contract.getFile() == null ? "无" : "有");
            contractDetail.setFile(contract.getFile());
            contractDetailList.add(contractDetail);

        }

        return contractDetailList;
    }

    @Override
    public Integer insertContract(ContractDetail param) {
        try {
            // 查询租客信息
            UserExample userExample = new UserExample();
            userExample.createCriteria().andEmailEqualTo(param.getRenterEmail());
            List<User> userList = userMapper.selectByExample(userExample);
            // 上传他合同

            String file = QiniuUtil.uploadFile(param.getContractFile());
            //修改状态
            HouseInfo houseInfo=new HouseInfo();
            houseInfo.setId(param.getHouseId());
            houseInfo.setStatus(2);
            houseInfoMapper.updateByPrimaryKeySelective(houseInfo);
            // 新增合同信息
            Contract contract = new Contract();
            contract.setRenterId(userList.get(0).getId());
            contract.setLandlordId(param.getLandlordId());
            contract.setHouseId(param.getHouseId());
            contract.setType(param.getType());
            contract.setStartTime(DateUtils.parseDateTime(param.getStartTimeStr()));
            contract.setEndTime(DateUtils.parseDateTime(param.getEndTimeStr()));
            contract.setFile(file);

            return contractMapper.insertSelective(contract);
        } catch (Exception e) {

        }
        return 0;
    }
}
