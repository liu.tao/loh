package com.loh.service.renter;

import com.loh.model.HouseInfo;
import com.loh.model.HouseInfoDetail;

import java.util.List;

/**
 * Created by liutao on 17-4-24.
 */
public interface ShowHouseService {
    List<HouseInfoDetail> showHouseDetail(HouseInfoDetail Param);
}
