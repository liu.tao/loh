package com.loh.service.renter.impl;

import com.google.common.collect.Lists;
import com.loh.dao.HouseInfoMapper;
import com.loh.model.HouseInfo;
import com.loh.model.HouseInfoDetail;
import com.loh.model.HouseInfoExample;
import com.loh.model.User;
import com.loh.service.admin.HouseManageService;
import com.loh.service.admin.UserManageService;
import com.loh.service.renter.ShowHouseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-24.
 */
@Service
public class ShowHouseServiceImpl implements ShowHouseService {

    @Resource
    private HouseInfoMapper houseInfoMapper;
    @Resource
    private UserManageService userManageService;
    @Resource
    private HouseManageService houseManageService;

    @Override
    public List<HouseInfoDetail> showHouseDetail(HouseInfoDetail param) {

        List<HouseInfoDetail> houseInfoDetailList = Lists.newArrayList();
        HouseInfoExample houseInfoExample = new HouseInfoExample();
        HouseInfoExample.Criteria criteria = houseInfoExample.createCriteria();
        criteria.andStatusEqualTo(1);
        criteria.andCheckedEqualTo(1);
        if (param.getRentMax() != null && param.getRentMin() != null) {
            criteria.andRentBetween(param.getRentMin(), param.getRentMax());
        }
        if (param.getChecked() != null) {
            criteria.andCheckedEqualTo(param.getChecked());
        }
        if (param.getLandlordId() != null) {
            criteria.andLandlordIdEqualTo(param.getLandlordId());
        }
        if (param.getHouseTypeId() != null) {
            criteria.andHouseTypeIdEqualTo(param.getHouseTypeId());
        }
        if (StringUtils.isNotEmpty(param.getAddress())) {
            criteria.andAddressLike("%" + param.getAddress() + "%");
        }

        List<HouseInfo> houseInfoList = houseInfoMapper.selectByExample(houseInfoExample);
        if (CollectionUtils.isEmpty(houseInfoList)) {
            return houseInfoDetailList;
        }
        for (HouseInfo houseInfo : houseInfoList) {
            HouseInfoDetail houseInfoDetail = new HouseInfoDetail();
            houseInfoDetail.setId(houseInfo.getId());
            houseInfoDetail.setAddress(houseInfo.getAddress());
            houseInfoDetail.setFloorSize(houseInfo.getFloorSize() + "平米");
            houseInfoDetail.setImageone(houseInfo.getImageone());
            houseInfoDetail.setImagetow(houseInfo.getImagetow());
            houseInfoDetail.setMaterial(houseInfo.getMaterial());
            houseInfoDetail.setCoordinate(houseInfo.getCoordinate());
            houseInfoDetail.setComment(houseInfo.getComment());
            houseInfoDetail.setRentStr(houseInfo.getRent() + "/月");
            houseManageService.setHouseType(houseInfoDetail, houseInfo);
            User user = userManageService.getUserById(houseInfo.getLandlordId());
            if (user != null) {
                houseInfoDetail.setLandlordName(user.getUsername());
                houseInfoDetail.setPhone(user.getPhone());
                houseInfoDetail.setEmail(user.getEmail());
            }
            houseInfoDetailList.add(houseInfoDetail);
        }
        return houseInfoDetailList;
    }
}
