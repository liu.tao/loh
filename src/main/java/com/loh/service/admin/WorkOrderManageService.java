package com.loh.service.admin;

import com.loh.model.WorkOrder;
import com.loh.model.WorkOrderInfo;

import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
public interface WorkOrderManageService {
    List<WorkOrderInfo> selectWorkOrder(WorkOrderInfo param);

    Integer replayWorkOrder(WorkOrder param);

    Integer insertWorkOrder(WorkOrder param);


}
