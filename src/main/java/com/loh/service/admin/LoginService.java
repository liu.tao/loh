package com.loh.service.admin;

import com.loh.model.User;

/**
 * Created by liutao on 17-4-16.
 */
public interface LoginService {
  Boolean login(User user);
}
