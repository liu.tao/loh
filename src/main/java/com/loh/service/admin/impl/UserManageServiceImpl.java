package com.loh.service.admin.impl;

import com.google.common.collect.Lists;
import com.loh.dao.LandlordInfoMapper;
import com.loh.dao.RenterInfoMapper;
import com.loh.dao.UserMapper;
import com.loh.model.*;
import com.loh.service.admin.UserManageService;

import com.loh.total.QiniuUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Service
public class UserManageServiceImpl implements UserManageService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private RenterInfoMapper renterInfoMapper;
    @Resource
    private LandlordInfoMapper landlordInfoMapper;

    @Override
    public List<UserInfo> selectRenter(User param) {
        List<UserInfo> userInfoList = Lists.newArrayList();
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andRoleIdEqualTo(2);
        if (param.getId() != null) {
            criteria.andIdEqualTo(param.getId());
        }
        if (param.getUsername() != null) {
            criteria.andUsernameLike("%" + param.getUsername() + "%");
        }
        List<User> userList = userMapper.selectByExample(userExample);

        for (User user : userList) {
            RenterInfo renterInfo = getRenterInfoByRenterId(user.getId());
            if (renterInfo == null) {
                continue;
            }
            UserInfo userInfo = new UserInfo();
            userInfo.setId(user.getId());
            userInfo.setRenterId(renterInfo.getRenterId());
            userInfo.setAddress(renterInfo.getAddress());
            if (user.getCertification() == 0) {
                userInfo.setCertificationStr("未认证");
            }
            if (user.getCertification() == 1) {
                userInfo.setCertificationStr("已认证");
            }
            if (user.getCertification() == 2) {
                userInfo.setCertificationStr("认证失败");
            }
            userInfo.setImageOne(renterInfo.getImageOne());
            userInfo.setImageTow(renterInfo.getImageTow());
            userInfo.setEmail(user.getEmail());
            userInfo.setUsername(user.getUsername());
            userInfo.setPhone(user.getPhone());
            userInfoList.add(userInfo);
        }
        return userInfoList;
    }

    private RenterInfo getRenterInfoByRenterId(Integer id) {
        RenterInfoExample renterInfoExample = new RenterInfoExample();
        renterInfoExample.createCriteria().andRenterIdEqualTo(id);
        List<RenterInfo> renterInfoList = renterInfoMapper.selectByExample(renterInfoExample);
        return CollectionUtils.isEmpty(renterInfoList) ? null : renterInfoList.get(0);
    }

    private LandlordInfo getLandlordInfoByRenterId(Integer id) {
        LandlordInfoExample landlordInfoExample = new LandlordInfoExample();
        landlordInfoExample.createCriteria().andLandlordIdEqualTo(id);
        List<LandlordInfo> landlordInfoList = landlordInfoMapper.selectByExample(landlordInfoExample);
        return CollectionUtils.isEmpty(landlordInfoList) ? null : landlordInfoList.get(0);
    }

    @Override
    public List<UserInfo> selectLandlord(User param) {
        List<UserInfo> userInfoList = Lists.newArrayList();
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andRoleIdEqualTo(3);
        if (param.getId() != null) {
            criteria.andIdEqualTo(param.getId());
        }
        if (param.getUsername() != null) {
            criteria.andUsernameLike("%" + param.getUsername() + "%");
        }
        List<User> userList = userMapper.selectByExample(userExample);
        for (User user : userList) {
            LandlordInfo landlordInfo = getLandlordInfoByRenterId(user.getId());
            if (landlordInfo == null) {
                continue;
            }
            UserInfo userInfo = new UserInfo();
            userInfo.setId(user.getId());
            userInfo.setRenterId(landlordInfo.getLandlordId());
            userInfo.setAddress(landlordInfo.getAddress());
            if (user.getCertification() == 0) {
                userInfo.setCertificationStr("未认证");
            }
            if (user.getCertification() == 1) {
                userInfo.setCertificationStr("已认证");
            }
            if (user.getCertification() == 2) {
                userInfo.setCertificationStr("认证失败");
            }
            userInfo.setImageOne(landlordInfo.getImageOne());
            userInfo.setImageTow(landlordInfo.getImageTow());
            userInfo.setEmail(user.getEmail());
            userInfo.setPhone(user.getPhone());
            userInfo.setUsername(user.getUsername());
            userInfoList.add(userInfo);
        }
        return userInfoList;
    }

    @Override
    public List<UserInfo> selectAdmin() {
        return null;
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer updateUser(User user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public Integer updateLandlord(UserInfo userInfo) {
        User user = new User();
        user.setId(userInfo.getId());
        user.setUsername(userInfo.getUsername());
        Integer count = updateUser(user);
        LandlordInfo landlordInfo = new LandlordInfo();
        landlordInfo.setAddress(userInfo.getAddress());
        landlordInfo.setLandlordId(userInfo.getId());
        LandlordInfoExample landlordInfoExample = new LandlordInfoExample();
        landlordInfoExample.createCriteria().andLandlordIdEqualTo(user.getId());
        return count + landlordInfoMapper.updateByExampleSelective(landlordInfo, landlordInfoExample);
    }

    @Override
    public Integer updateRent(UserInfo userInfo) {
        User user = new User();
        user.setId(userInfo.getId());
        user.setUsername(userInfo.getUsername());
        Integer count = updateUser(user);
        RenterInfo renterInfo = new RenterInfo();
        renterInfo.setAddress(userInfo.getAddress());
        RenterInfoExample renterInfoExample = new RenterInfoExample();
        renterInfoExample.createCriteria().andRenterIdEqualTo(userInfo.getId());
        return count + renterInfoMapper.updateByExampleSelective(renterInfo, renterInfoExample);
    }

    @Override
    public User getUSerByCriteria(User param) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andEmailEqualTo(param.getEmail());
        criteria.andPasswordEqualTo(param.getPassword());
        criteria.andRoleIdEqualTo(param.getRoleId());
        List<User> userList = userMapper.selectByExample(userExample);
        if (!CollectionUtils.isEmpty(userList) && userList.size() == 1) {

            return userList.get(0);
        }
        return null;
    }

    // 上传租客证件验证资料
    @Override
    public Integer uploadRentCertificationImage(UserInfo userInfo) {

        RenterInfo renterInfo = new RenterInfo();
        renterInfo.setRenterId(userInfo.getId());
        if (userInfo.getImageOneFile() != null) {
            renterInfo.setImageOne(QiniuUtil.uploadFile(userInfo.getImageOneFile()));
        }
        if (userInfo.getImageTowFile() != null) {
            renterInfo.setImageTow(QiniuUtil.uploadFile(userInfo.getImageTowFile()));
        }
        RenterInfoExample renterInfoExample = new RenterInfoExample();

        renterInfoExample.createCriteria().andRenterIdEqualTo(userInfo.getId());
        return renterInfoMapper.updateByExampleSelective(renterInfo, renterInfoExample);
    }

    // 上传房东证件验证资料
    @Override
    public Integer uploadLandlordCertificationImage(UserInfo userInfo) {

        LandlordInfo landlordInfo = new LandlordInfo();
        landlordInfo.setLandlordId(userInfo.getId());
        if (userInfo.getImageOneFile() != null) {
            landlordInfo.setImageOne(QiniuUtil.uploadFile(userInfo.getImageOneFile()));
        }
        if (userInfo.getImageTowFile() != null) {
            landlordInfo.setImageTow(QiniuUtil.uploadFile(userInfo.getImageTowFile()));
        }
        LandlordInfoExample landlordInfoExample = new LandlordInfoExample();
        landlordInfoExample.createCriteria().andLandlordIdEqualTo(userInfo.getId());
        return landlordInfoMapper.updateByExampleSelective(landlordInfo, landlordInfoExample);
    }
}
