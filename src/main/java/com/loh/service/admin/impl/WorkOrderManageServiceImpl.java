package com.loh.service.admin.impl;

import com.google.common.collect.Lists;
import com.loh.dao.WorkOrderMapper;
import com.loh.model.WorkOrder;
import com.loh.model.WorkOrderExample;
import com.loh.model.WorkOrderInfo;
import com.loh.service.admin.UserManageService;
import com.loh.service.admin.WorkOrderManageService;
import com.loh.total.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Service
public class WorkOrderManageServiceImpl implements WorkOrderManageService {

    @Resource
    private WorkOrderMapper workOrderMapper;
    @Resource
    private UserManageService userManageService;

    @Override
    public List<WorkOrderInfo> selectWorkOrder(WorkOrderInfo param) {
        List<WorkOrderInfo> workOrderInfoList = Lists.newArrayList();
        WorkOrderExample workOrderExample = new WorkOrderExample();
        WorkOrderExample.Criteria criteria = workOrderExample.createCriteria();
        if (param.getStatus() != null) {
            criteria.andStatusEqualTo(param.getStatus());
        }
        if (param.getId() != null) {
            criteria.andIdEqualTo(param.getId());
        }
        if (param.getUserId() != null) {
            criteria.andUserIdEqualTo(param.getUserId());
        }
        criteria.andUserIdIsNotNull();
        List<WorkOrder> workOrderList = workOrderMapper.selectByExample(workOrderExample);
        if (workOrderList == null) {
            return workOrderInfoList;
        }
        for (WorkOrder workOrder : workOrderList) {
            WorkOrderInfo workOrderInfo = new WorkOrderInfo();
            if (workOrder.getStatus() == 1) {
                workOrderInfo.setReplyStatus("已回复");
            } else {
                workOrderInfo.setReplyStatus("未回复");
            }
            String username = userManageService.getUserById(workOrder.getUserId()).getUsername();
            workOrderInfo.setUsername(username);
            workOrderInfo.setCreateTimeShow(DateUtils.format(workOrder.getCreateTime()));
            workOrderInfo.setId(workOrder.getId());
            workOrderInfo.setComment(workOrder.getComment());
            if (StringUtils.isNotEmpty(workOrder.getReply())) {
                workOrderInfo.setReply(workOrder.getReply());
            }
            if (workOrder.getReplyTime() != null) {
                workOrderInfo.setReplyTimeShow(DateUtils.format(workOrder.getReplyTime()));
            }
            workOrderInfoList.add(workOrderInfo);
        }
        return workOrderInfoList;
    }

    @Override
    public Integer replayWorkOrder(WorkOrder param) {
        param.setReplyTime(new Date());
        param.setStatus(1);

        return workOrderMapper.updateByPrimaryKeySelective(param);
    }

    @Override
    public Integer insertWorkOrder(WorkOrder param) {
        param.setCreateTime(new Date());
        param.setStatus(0);
        return workOrderMapper.insertSelective(param);
    }
}
