package com.loh.service.admin.impl;

import com.google.common.collect.Lists;
import com.loh.dao.HouseInfoMapper;
import com.loh.dao.HouseTypeMapper;
import com.loh.dao.UserMapper;
import com.loh.model.*;
import com.loh.service.admin.HouseManageService;
import com.loh.service.admin.UserManageService;
import com.loh.total.QiniuUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Service
public class HouseManageServiceImpl implements HouseManageService {
    @Resource
    private HouseInfoMapper houseInfoMapper;
    @Resource
    private HouseTypeMapper houseTypeMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private UserManageService userManageService;

    @Override
    public List<HouseInfoDetail> selectHouseInfoDetail(HouseInfoDetail param) {
        List<HouseInfoDetail> houseInfoDetailList = Lists.newArrayList();
        HouseInfoExample houseInfoExample = new HouseInfoExample();
        HouseInfoExample.Criteria criteria = houseInfoExample.createCriteria();
        if (param.getStatus() != null) {
            criteria.andStatusEqualTo(param.getStatus());
        }
        if (param.getChecked() != null) {
            criteria.andCheckedEqualTo(param.getChecked());
        }
        if (param.getLandlordId() != null) {
            criteria.andLandlordIdEqualTo(param.getLandlordId());
        }
        if (StringUtils.isNotEmpty(param.getAddress())) {
            criteria.andAddressLike("%" + param.getAddress() + "%");
        }

        List<HouseInfo> houseInfoList = houseInfoMapper.selectByExample(houseInfoExample);
        if (CollectionUtils.isEmpty(houseInfoList)) {
            return houseInfoDetailList;
        }
        for (HouseInfo houseInfo : houseInfoList) {
            HouseInfoDetail houseInfoDetail = new HouseInfoDetail();
            houseInfoDetail.setId(houseInfo.getId());
            houseInfoDetail.setAddress(houseInfo.getAddress());
            houseInfoDetail.setFloorSize(houseInfo.getFloorSize() + "平米");
            if (houseInfo.getChecked() == 1) {
                houseInfoDetail.setIsChecked("通过");
            }
            if (houseInfo.getChecked() == 0) {
                houseInfoDetail.setIsChecked("待审核");
            }
            if (houseInfo.getChecked() == 2) {
                houseInfoDetail.setIsChecked("未通过");
            }
            houseInfoDetail.setRent(houseInfo.getRent());
            if (2==houseInfo.getStatus()) {
                houseInfoDetail.setStatusStr("已租出");
            } else {
                houseInfoDetail.setStatusStr("空闲");
            }
            setHouseType(houseInfoDetail, houseInfo);
            User user = userManageService.getUserById(houseInfo.getLandlordId());
            if (user != null) {
                houseInfoDetail.setLandlordName(user.getUsername());
            }
            houseInfoDetailList.add(houseInfoDetail);
        }
        return houseInfoDetailList;
    }

    public void setHouseType(HouseInfoDetail houseInfoDetail, HouseInfo houseInfo) {
        if (houseInfo.getHouseTypeId() == null) {
            return;
        }
        HouseType houseType = houseTypeMapper.selectByPrimaryKey(houseInfo.getHouseTypeId());
        if (houseType != null) {
            houseInfoDetail.setHouseType(houseType.getName());
        }
    }

    @Override
    public List<HouseType> selectHouseTypeList() {
        return houseTypeMapper.selectByExample(null);
    }

    @Override
    public Integer updateHouseInfo(HouseInfoDetail param) {
        HouseInfo houseInfo = new HouseInfo();
        houseInfo.setId(param.getId());
        houseInfo.setChecked(param.getChecked());
        return houseInfoMapper.updateByPrimaryKeySelective(houseInfo);
    }

    @Override
    public Integer uploadMaterial(HouseInfoDetail houseInfoDetail) {
        HouseInfo houseInfo = new HouseInfo();
        houseInfo.setId(houseInfoDetail.getId());
        String imageOneName;
        String imageTowName;
        String fullImageName;
        if (houseInfoDetail.getImageOneFile() != null) {
            imageOneName = QiniuUtil.uploadFile(houseInfoDetail.getImageOneFile());
            houseInfo.setImageone(imageOneName);

        }
        if (houseInfoDetail.getImageTowFile() != null) {
            imageTowName = QiniuUtil.uploadFile(houseInfoDetail.getImageTowFile());
            houseInfo.setImagetow(imageTowName);
        }
        if (houseInfoDetail.getFullImageFile() != null) {
            fullImageName = QiniuUtil.uploadFile(houseInfoDetail.getFullImageFile());
            houseInfo.setMaterial(fullImageName);
        }

        return houseInfoMapper.updateByPrimaryKeySelective(houseInfo);
    }

    @Override
    public Integer insertHouseInfo(HouseInfoDetail houseInfoDetail) {
        HouseInfo houseInfo = new HouseInfo();
        try {
            houseInfo.setLandlordId(houseInfoDetail.getLandlordId());
            houseInfo.setHouseTypeId(houseInfoDetail.getHouseTypeId());
            houseInfo.setAddress(houseInfoDetail.getAddress());
            houseInfo.setComment(houseInfoDetail.getComment());
            houseInfo.setFloorSize(houseInfoDetail.getFloorSize());
            houseInfo.setChecked(0);
            houseInfo.setStatus(1);
            houseInfo.setRent(houseInfoDetail.getRent());
        }catch (Exception e){

        }
        return houseInfoMapper.insertSelective(houseInfo);
    }

}
