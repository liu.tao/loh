package com.loh.service.admin.impl;

import com.loh.dao.UserMapper;
import com.loh.model.User;
import com.loh.model.UserExample;
import com.loh.service.admin.LoginService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Resource
    private UserMapper userMapper;

    @Override
    public Boolean login(User param) {
        boolean status = false;
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andEmailEqualTo(param.getEmail());
        criteria.andPasswordEqualTo(param.getPassword());
        criteria.andRoleIdEqualTo(param.getRoleId());
        List<User> userList = userMapper.selectByExample(userExample);
        if (!CollectionUtils.isEmpty(userList) && userList.size() == 1) {
            status = true;
        }
        return status;
    }

}
