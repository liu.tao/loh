package com.loh.service.admin.impl;

import com.loh.dao.ContractTypeMapper;
import com.loh.model.ContractType;
import com.loh.service.admin.ContractManageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
@Service
public class ContractManageServiceImpl implements ContractManageService {
    @Resource
    private ContractTypeMapper contractTypeMapper;

    @Override
    public List<ContractType> selectContractType() {

        return contractTypeMapper.selectByExample(null);
    }

    @Override
    public ContractType selectContractTypeById(Integer id) {

        return contractTypeMapper.selectByPrimaryKey(id);
    }
}
