package com.loh.service.admin;

import com.loh.model.HouseInfo;
import com.loh.model.HouseInfoDetail;
import com.loh.model.HouseType;
import com.loh.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
public interface HouseManageService {
    List<HouseInfoDetail> selectHouseInfoDetail(HouseInfoDetail param);

    void setHouseType(HouseInfoDetail houseInfoDetail, HouseInfo houseInfo);


    List<HouseType> selectHouseTypeList();

    Integer updateHouseInfo(HouseInfoDetail param);

    //上传材料
    Integer uploadMaterial(HouseInfoDetail houseInfoDetail);

    //提交房屋申请
    Integer insertHouseInfo(HouseInfoDetail houseInfoDetail);
}
