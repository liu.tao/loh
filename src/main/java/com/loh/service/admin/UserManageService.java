package com.loh.service.admin;

import com.loh.model.User;
import com.loh.model.UserInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */

public interface UserManageService {
    List<UserInfo> selectRenter(User param);

    List<UserInfo> selectLandlord(User param);

    List<UserInfo> selectAdmin();

    Integer updateLandlord(UserInfo userInfo);

    Integer updateRent(UserInfo userInfo);

    Integer updateUser(User user);

    User getUserById(Integer id);

    User getUSerByCriteria(User user);

    Integer uploadRentCertificationImage(UserInfo userInfo);

    Integer uploadLandlordCertificationImage(UserInfo userInfo);

    

}
