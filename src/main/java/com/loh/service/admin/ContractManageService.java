package com.loh.service.admin;

import com.loh.model.ContractType;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * Created by liutao on 17-4-16.
 */
public interface ContractManageService {
 List<ContractType>  selectContractType();

 ContractType selectContractTypeById(Integer id);
}
