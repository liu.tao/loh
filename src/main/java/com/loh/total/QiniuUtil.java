package com.loh.total;

import com.alibaba.fastjson.JSON;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by liutao on 17-5-6.
 */
public class QiniuUtil {
    // 设置好账号的ACCESS_KEY和SECRET_KEY
    private static String ACCESS_KEY = "cujPqw7Y9ZzGmMeY1NuC83Bq1THUchrWH3kp7tG_"; // 这两个登录七牛 账号里面可以找到
    private static String SECRET_KEY = "j-URu_gRdvjlu7MPkINDNsJcrmjCcE0KbdymtCwG";

    // 要上传的空间
    private static String bucketname = "lohsystem"; // 对应要上传到七牛上 你的那个路径（自己建文件夹 注意设置公开）

    // 简单上传，使用默认策略，只需要设置上传的空间名就可以了
    private static String getUpToken() {
        // 密钥配置
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        return auth.uploadToken(bucketname);
    }

    // 普通上传
    public static String upload(byte[] file, String fileame) throws IOException {
        try {
            // 创建上传对象
            UploadManager uploadManager = new UploadManager();
            // 调用put方法上传
            Response res = uploadManager.put(file, fileame, getUpToken());
            // 返回文件名
            return res.bodyString();
            // 打印返回的信息
        } catch (QiniuException e) {
            Response r = e.response;
            // 请求失败时打印的异常的信息
            System.out.println(r.toString());
        }
        return null;
    }

    public static String uploadFile(MultipartFile file) {
        try {
            String url = "http://oojaq5nk3.bkt.clouddn.com/";
            String resultPath = upload(file.getBytes(), file.getOriginalFilename());
            return url + JSON.parseObject(resultPath).get("key").toString();
        } catch (Exception e) {

        }
        return null;
    }

}
