package com.loh.total;

/**
 * 扩展commons的NumberUtils
 * Created by sulin on 16/6/6.
 */
public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils {

    /**
     * 将参数字符串str以指定分隔符切割，并将获得的string[]转化为int[]
     * 非int将会被转换为null
     *
     * @param str           参数字符串，如1,2,3,4
     * @param separatorChar 分隔符，如,
     * @return [1, 2, 3, 4]
     */
    public static Integer[] splitToInteger(String str, char separatorChar) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        String[] numStrs = StringUtils.split(str, separatorChar);
        Integer[] result = new Integer[numStrs.length];
        for (int i = 0; i < numStrs.length; i++) {
            try {
                result[i] = Integer.parseInt(numStrs[i]);
            } catch (Exception e) {
                result[i] = null;
            }
        }
        return result;
    }

}
