package com.loh.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by liutao on 17-4-16.
 */
public class UserInfo extends User {

    private Integer landlordId;
    private Integer renterId;
    private String imageTow;
    private String imageOne;
    private String certificationStr;
    private String address;

    private MultipartFile imageOneFile;
    private MultipartFile imageTowFile;

    public Integer getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(Integer landlordId) {
        this.landlordId = landlordId;
    }

    public Integer getRenterId() {
        return renterId;
    }

    public void setRenterId(Integer renterId) {
        this.renterId = renterId;
    }

    public String getImageTow() {
        return imageTow;
    }

    public void setImageTow(String imageTow) {
        this.imageTow = imageTow;
    }

    public String getImageOne() {
        return imageOne;
    }

    public void setImageOne(String imageOne) {
        this.imageOne = imageOne;
    }

    public String getCertificationStr() {

        return certificationStr;
    }

    public void setCertificationStr(String certificationStr) {
        this.certificationStr = certificationStr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MultipartFile getImageOneFile() {
        return imageOneFile;
    }

    public void setImageOneFile(MultipartFile imageOneFile) {
        this.imageOneFile = imageOneFile;
    }

    public MultipartFile getImageTowFile() {
        return imageTowFile;
    }

    public void setImageTowFile(MultipartFile imageTowFile) {
        this.imageTowFile = imageTowFile;
    }
}
