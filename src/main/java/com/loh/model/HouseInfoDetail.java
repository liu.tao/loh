package com.loh.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by liutao on 17-4-17.
 */
public class HouseInfoDetail extends HouseInfo {

    // landlordInfo
    private String landlordName;
    private String phone;
    private String email;
    // house_type
    private String houseType;
    private Integer houseTypeId;
    // house
    private String isChecked;
    private String rentStr;
    private String statusStr;

    // param
    private Integer rentMax = 10000;
    private Integer rentMin = 100;
    // select
    private String key; // 关键字查询
    // 上传文件
    private MultipartFile imageOneFile;
    private MultipartFile imageTowFile;
    private MultipartFile fullImageFile;

    public String getLandlordName() {
        return landlordName;
    }

    public void setLandlordName(String landlordName) {
        this.landlordName = landlordName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    @Override
    public Integer getHouseTypeId() {
        return houseTypeId;
    }

    public void setHouseTypeId(Integer houseTypeId) {
        this.houseTypeId = houseTypeId;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRentMax() {
        return rentMax;
    }

    public void setRentMax(Integer rentMax) {
        this.rentMax = rentMax;
    }

    public Integer getRentMin() {
        return rentMin;
    }

    public void setRentMin(Integer rentMin) {
        this.rentMin = rentMin;
    }

    public String getRentStr() {
        return rentStr;
    }

    public void setRentStr(String rentStr) {
        this.rentStr = rentStr;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public MultipartFile getImageOneFile() {
        return imageOneFile;
    }

    public void setImageOneFile(MultipartFile imageOneFile) {
        this.imageOneFile = imageOneFile;
    }

    public MultipartFile getImageTowFile() {
        return imageTowFile;
    }

    public void setImageTowFile(MultipartFile imageTowFile) {
        this.imageTowFile = imageTowFile;
    }

    public MultipartFile getFullImageFile() {
        return fullImageFile;
    }

    public void setFullImageFile(MultipartFile fullImageFile) {
        this.fullImageFile = fullImageFile;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
