package com.loh.model;

/**
 * Created by liutao on 17-4-19.
 */
public class LeaseInfo extends ContractDetail {
    private String renterEmail;
    private String status;

    public String getRenterEmail() {
        return renterEmail;
    }

    public void setRenterEmail(String renterEmail) {
        this.renterEmail = renterEmail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
