package com.loh.model;

/**
 * Created by liutao on 17-4-17.
 */
public class WorkOrderInfo extends WorkOrder {
    //
    private String username;
    private String replyStatus;
    private String createTimeShow;
    private String replyTimeShow;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }



    public String getCreateTimeShow() {
        return createTimeShow;
    }

    public void setCreateTimeShow(String createTimeShow) {
        this.createTimeShow = createTimeShow;
    }

    public String getReplyTimeShow() {
        return replyTimeShow;
    }

    public void setReplyTimeShow(String replyTimeShow) {
        this.replyTimeShow = replyTimeShow;
    }
}
